# Recipe App API Proxy

NGINX proxy for Recipe app API

## Usage

### ENV variables

 * `LISTEN_PORT` - Port to listen on  (default: 8000)
 * `APP_HOST` - Host of the app to forward requests to (default: `app`)
 * `APP_PORT` - Post of the app to forward requests to (default: `9000`)
